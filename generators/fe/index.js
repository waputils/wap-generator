(() => {
    'use strict';

    const Generator = require('yeoman-generator');

    class frontEndGenerator extends Generator {
        constructor(args, opts) {
            super(args, opts);
            this.templateTypeOptions = ['component', 'directive', 'service', 'factory', 'module', 'component_es6', 'directive_es6', 'service_es6', 'factory_es6', 'component_w4_es6'];

            this.argument('templateType', { type: String, required: false });
            this.argument('moduleName', { type: String, required: false });
            this.argument('componentName', { type: String, required: false });
        }
        initializing() {
            this.generatorConfig = {};
        }
        prompting() {
            const userPromts = [];

            if (!this.options.templateType) {
                userPromts.push({
                    type: 'list',
                    name: 'templateType',
                    choices: this.templateTypeOptions,
                    message: 'type of template:'
                });
            }

            if (!this.options.moduleName) {
                userPromts.push({
                    type: 'input',
                    name: 'moduleName',
                    message: 'module name:'
                });
            }

            if (!this.options.componentName) {
                userPromts.push({
                    type: 'input',
                    name: 'componentName',
                    message: "component name (if 'module' is selected this is used for the base component):"
                });
            }

            return this.prompt(userPromts).then((answers) => {
                this.generatorConfig.templateType = this.options.templateType ? this.options.templateType : answers.templateType;

                if (this.templateTypeOptions.indexOf(this.generatorConfig.templateType) === -1) {
                    this.log.error('Incorrect template type.');
                    process.exit();
                    return;
                }

                this.generatorConfig.moduleName = this.options.moduleName ? this.options.moduleName : answers.moduleName;
                this.generatorConfig.componentName = this.options.componentName ? this.options.componentName : answers.componentName;

                try {
                    this.composeWith(require.resolve(`../../subgenerators/${this.generatorConfig.templateType}`), {
                        arguments: [this.generatorConfig.moduleName, this.generatorConfig.componentName]
                    });
                } catch (err) {
                    this.log.error('Can not find a module for this template type.');
                    process.exit();
                }
            });
        }
    }

    module.exports = frontEndGenerator;
})();
