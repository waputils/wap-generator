(() => {
    'use strict';

    const Generator = require('yeoman-generator');
    const _ = require('lodash');

    class frontEndGenerator extends Generator {
        constructor(args, opts) {
            super(args, opts);

            this.argument('moduleName', { type: String, required: false });
            this.argument('moduleDescription', { type: String, required: false });
            this.argument('port', { type: String, required: false });
        }
        initializing() {
            this.generatorConfig = {};

            this.templatesConfigs = [{
                templatePath: 'gitignore',
                destinationPath: '.gitignore'
            }, {
                templatePath: 'package.json',
                destinationPath: 'package.json'
            }, {
                templatePath: 'server.config.json',
                destinationPath: 'server.config.json'
            }, {
                templatePath: 'server.js',
                destinationPath: 'server.js'
            }, {
                templatePath: 'routes/index.js',
                destinationPath: 'routes/index.js'
            }, {
                templatePath: 'routes/healthCheck.js',
                destinationPath: 'routes/healthCheck.js'
            }];
        }
        prompting() {
            const userPromts = [];

            if (!this.options.moduleName) {
                userPromts.push({
                    type: 'input',
                    name: 'moduleName',
                    message: 'Microservice name:'
                });
            }

            if (!this.options.moduleDescription) {
                userPromts.push({
                    type: 'input',
                    name: 'moduleDescription',
                    message: 'Microservice description:'
                });
            }

            if (!this.options.port) {
                userPromts.push({
                    type: 'input',
                    name: 'port',
                    message: 'Microservice port:'
                });
            }

            return this.prompt(userPromts).then((answers) => {
                this.generatorConfig.moduleName = this.options.moduleName ? this.options.moduleName : answers.moduleName;
                this.generatorConfig.moduleDescription = this.options.moduleDescription ? this.options.moduleDescription : answers.moduleDescription;
                this.generatorConfig.port = this.options.port ? this.options.port : answers.port;
            });
        }
        writing() {
            const replaceConfig = _.clone(this.generatorConfig);
            
            this.templatesConfigs.forEach((config) => {
                this.fs.copyTpl(
                    this.templatePath(`${this.sourceRoot()}/${config.templatePath}`),
                    this.destinationPath(`${this.destinationRoot()}/${config.destinationPath}`),
                    replaceConfig
                );
            });
        }
        install() {
            this.npmInstall();
        }
    }

    module.exports = frontEndGenerator;
})();
