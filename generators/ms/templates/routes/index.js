(() => {
    'use strict';
    
    const express = require('express');
    const healthCheck = require('./healthCheck');    
    const requestErrorHandler = require('service_utils').requestErrorHandler;

    const router = express.Router();
    
    router.use('(public)?/health-check', healthCheck);
    
    router.use(requestErrorHandler);    

    module.exports = router;
})();