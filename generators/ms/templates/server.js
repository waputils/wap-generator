(() => {
    'use strict';
    
    const _ = require('lodash');
    const bodyParser = require('body-parser');
    const configZookeeperService = require('service_utils').configZookeeperService;
    const express = require('express');
    const mongoose = require('mongoose');
    const mongooseConnectionService = require('service_utils').mongooseConnectionService;
    const routeZookeeperService = require('service_utils').routeZookeeperService;
    const serverConfig = require('./server.config.json');

    const app = express();

    // Set default mongoose promise library to q as their native promise library is depricated.
    mongoose.Promise = require('q').Promise;

    // Configure bodyparser and override the default request size.
    app.use(bodyParser.json({ limit: '50mb' }));
    app.use(bodyParser.urlencoded({ extended: false, parameterLimit: 100000, limit: '50mb' }));
    // Register routes.    
    app.use(require('./routes'));
    // Start Express server on port.
    app.listen(serverConfig.app.port, () => {
        console.info(`<%= moduleName %> server running on port ${serverConfig.app.port}`);
    });

    // Initialize zookeeperRouteService
    const routeZookeeperServiceOptions = {
        config: serverConfig.zookeeper,
        zookeeperNode: '<%= moduleName %>',
        servicePort: serverConfig.app.port,
        data: app._router.stack
    };

    routeZookeeperService.init(routeZookeeperServiceOptions);

    const configZookeeperServiceOptions = {
        config: serverConfig.zookeeper,
        createNewNode: false,
        getDataCallback: (data) => {
            if (!_.isEqual(data, {})) {
                mongooseConnectionService.closeConnection();
                mongooseConnectionService.init(data.wapConfigs.mongoose);
            }
        }
    };

    configZookeeperService.init(configZookeeperServiceOptions);

    function shutDown() {
        // Clean up and close all connections.
        routeZookeeperService.closeConnection();
        configZookeeperService.closeConnection();
        mongooseConnectionService.closeConnection();
        // Exit nodeJs.
        process.exit();
    }

    process.on('SIGINT', shutDown);
    process.on('SIGTERM', shutDown);
    process.on('uncaughtException', shutDown);
    process.on('exit', shutDown);
})();