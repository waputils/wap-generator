(() => {
    'use strict';

    const Generator = require('yeoman-generator');
    const convertToKebabCase = require('../helpers/convertCamelToKebab');
    const _ = require('lodash');

    class frontEndComponents extends Generator {
        constructor(args, opts) {
            super(args, opts);

            this.argument('moduleName', { type: String, required: true });
            this.argument('componentName', { type: String, required: true });
        }

        initialize() {
            this.defaultTemplateConfig = {
                replace: {
                    moduleName: this.options.moduleName,
                    componentName: this.options.componentName,
                    className: this.options.componentName[0].toUpperCase() + this.options.componentName.substring(1)
                }
            };
        }

        writing() {
            this.templatesConfigs.forEach((config) => {
                const replaceConfig = _.clone(this.defaultTemplateConfig.replace);
                
                _.merge(replaceConfig, config.replace);

                if (replaceConfig.templateUrl) {
                    replaceConfig.templateUrl = `${this.destinationRoot()}/${convertToKebabCase(this.options.componentName)}/${replaceConfig.templateUrl}`.split('app/')[1] || `${this.destinationRoot()}/${convertToKebabCase(this.options.componentName)}/${replaceConfig.templateUrl}`;
                }

                this.fs.copyTpl(
                    this.templatePath(`${this.sourceRoot()}/${config.templatePath}`),
                    this.destinationPath(`${this.destinationRoot()}/${convertToKebabCase(this.options.componentName)}/${convertToKebabCase(config.destinationPath)}`),
                    replaceConfig
                );
            });
        }
    }

    module.exports = frontEndComponents;
})();
