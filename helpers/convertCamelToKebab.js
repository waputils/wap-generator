// Copied furiously from: https://gist.github.com/nblackburn/875e6ff75bc8ce171c758bf75f304707
module.exports = (string) => {
    return string.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
};