(() => {
    'use strict';

    const frontEndModuleGenerator = require('../../baseClasses/frontEndComponents');

    class moduleGenerator extends frontEndModuleGenerator {
        initialize() {
            super.initialize();

            this.templatesConfigs = [{
                templatePath: 'index.js',
                destinationPath: 'index.js'
            }, {
                templatePath: '_index.scss',
                destinationPath: '_index.scss'
            }, {
                templatePath: 'common/components/components.module.js',
                destinationPath: 'common/components/components.module.js'
            }, {
                templatePath: 'common/directives/directives.module.js',
                destinationPath: 'common/directives/directives.module.js'
            }, {
                templatePath: 'common/filters/filters.module.js',
                destinationPath: 'common/filters/filters.module.js'
            }, {
                templatePath: 'common/services/services.module.js',
                destinationPath: 'common/services/services.module.js'
            }, {
                templatePath: 'components/components.module.js',
                destinationPath: 'components/components.module.js'
            }, {
                templatePath: 'components/baseComponent/baseComponent.module.js',
                destinationPath: `components/${this.options.componentName}/${this.options.componentName}.module.js`
            }, {
                templatePath: 'components/baseComponent/baseComponent.component.js',
                destinationPath: `components/${this.options.componentName}/${this.options.componentName}.component.js`
            }, {
                templatePath: 'components/baseComponent/baseComponent.html',
                destinationPath: `components/${this.options.componentName}/${this.options.componentName}.html`
            }, {
                templatePath: 'components/baseComponent/_baseComponent.scss',
                destinationPath: `components/${this.options.componentName}/_${this.options.componentName}.scss`
            }];
        }
        writing() {
            super.writing();
        }
    }

    module.exports = moduleGenerator;
})();
