(function() {
    'use strict';
    
    // Require all component module files.
    require('./<%= componentName %>/<%= componentName %>.module.js');

    angular.module('<%= moduleName %>.components', [
        '<%= moduleName %>.components.<%= componentName %>'
    ]);
})();