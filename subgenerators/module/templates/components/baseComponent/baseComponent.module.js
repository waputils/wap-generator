(function() {
    'use strict';

    angular.module('<%= moduleName %>.components.<%= componentName %>', []);
    
    require('./<%= componentName %>.component.js');
})();