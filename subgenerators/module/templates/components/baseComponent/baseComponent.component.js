(function() {
    'use strict';

    angular.module('<%= moduleName %>.components.<%= componentName %>').component('<%= componentName %>', {
        templateUrl: 'features/<%= moduleName %>/components/<%= componentName %>/<%= componentName %>.html',
        controller: <%= componentName %>Controller,
        controllerAs: 'Ctrl',
        bindings: {
            userModulePermissions: '<'
        }
    });

    <%= componentName %>Controller.$inject = [];

    function <%= componentName %>Controller() {
        var self = this;

    }
})();
