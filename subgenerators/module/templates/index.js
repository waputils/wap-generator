(function() {
    'use strict';

    // Require all modules you want to use in your dependency injection.
    require('./common/services/services.module.js');
    require('./common/directives/directives.module.js');
    require('./common/filters/filters.module.js');
    require('./common/components/components.module.js');
    require('./components/components.module.js');

    var dependencies = [
        'ui.router',
        '<%= moduleName %>.services',
        '<%= moduleName %>.directives',
        '<%= moduleName %>.filters',
        '<%= moduleName %>.commonComponents',
        '<%= moduleName %>.components'
    ];

    angular.module('<%= moduleName %>', dependencies).config(config);

    config.$inject = ['$stateProvider'];

    function config($stateProvider) {
        $stateProvider.state('main.<%= componentName %>', {
            'abstract': true,
            template: '<ui-view />',
            data: {
                keyPageName: '<%= moduleName %>'
            },
            resolve: {
                userModulePermissions: ['userService', function(userService) {
                    return userService.getUserModulePermissions('<%= moduleName %>');
                }]
            }
        }).state('main.<%= componentName %>.overview', {
            url: '/<%= moduleName %>',
            component: '<%= componentName %>'
        });
    }
})();