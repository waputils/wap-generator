(function() {
    'use strict';
    
    angular.module('<%= moduleName %>.directives', []);
    
    // Require common directives here

})();