(function() {
    'use strict';
    
    angular.module('<%= moduleName %>.services', []);
    
    // Require common services here

})();