(function() {
    'use strict';
    
    angular.module('<%= moduleName %>.commonComponents', []);
    
    // Require common components here
    
})();