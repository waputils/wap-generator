(function() {
    'use strict';

    angular.module('<%= moduleName %>').service('<%= componentName %>', <%= componentName %>);

    <%= componentName %>.$inject = [];

    function <%= componentName %>() {
        var self = this;
        
    }
})();