(() => {
    'use strict';

    const frontEndModuleGenerator = require('../../baseClasses/frontEndComponents');

    class componentGenerator extends frontEndModuleGenerator {
        constructor(args, opts) {
            super(args, opts);
        }
        initialize() {
            super.initialize();

            this.templatesConfigs = [{
                templatePath: 'service.js',
                destinationPath: `${this.options.componentName}.service.js`
            }];
        }
        writing() {
            super.writing();
        }
    }

    module.exports = componentGenerator;
})();
