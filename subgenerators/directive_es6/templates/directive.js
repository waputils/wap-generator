(() => {
    'use strict';

    angular
        .module('<%= moduleName %>')
        .directive('<%= componentName %>', () => {
            'ngInject';

            class <%= className %> {
                constructor() {
                    this.restrict = 'A';
                    this.scope = {};
                    
                    throw new Error('NotImplemented');
                }

                link() {
                    throw new Error('NotImplemented');
                }
            }

            return new <%= className %>();
        });
})();
