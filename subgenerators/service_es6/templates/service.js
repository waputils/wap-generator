(() => {
    'use strict';

    angular
        .module('<%= moduleName %>')
        .service('<%= componentName %>', () => {
            'ngInject';

            class <%= className %> {
                constructor() {
                    throw new Error('NotImplemented');
                }
            }

            return new <%= className %>();
        });
})();