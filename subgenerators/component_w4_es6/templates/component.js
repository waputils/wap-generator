import <%= componentName %>Tpl from './<%= url %>.html';
import './<%= url %>.scss';

export default {
    template: <%= componentName %>Tpl,
    controllerAs: 'Ctrl',
    bindings: {
    },
    controller: () => {
        'ngInject';

        class <%= className %>Controller {
            constructor() {
                throw new Error('NotImplemented');
            }
        }

        return new <%= className %>Controller();
    }
}