import <%= componentName %>Component from './<%= componentPath %>';

const moduleName = '<%= moduleName %>';
angular
    .module(moduleName, [])
    .component('<%= componentName %>', <%= componentName %>Component);

export { moduleName as default };