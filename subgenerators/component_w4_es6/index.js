(() => {
    'use strict';

    const frontEndModuleGenerator = require('../../baseClasses/frontEndComponents');
    const convertToKebabCase = require('../../helpers/convertCamelToKebab');

    class componentGenerator extends frontEndModuleGenerator {
        initialize() {
            super.initialize();

            this.templatesConfigs = [{
                templatePath: 'component.js',
                destinationPath: `${this.options.componentName}.component.js`,
                replace: {
                    templateUrl: `${convertToKebabCase(this.options.componentName)}.html`,
                    url: `${convertToKebabCase(this.options.componentName)}`
                }
            }, {
                templatePath: 'template.html',
                destinationPath: `${this.options.componentName}.html`,
                replace: {
                    componentName: convertToKebabCase(this.options.componentName)
                }
            }, {
                templatePath: 'styles.scss',
                destinationPath: `${convertToKebabCase(this.options.componentName)}.scss`,
                replace: {
                    componentName: convertToKebabCase(this.options.componentName)
                }
            }, {
                templatePath: 'module.js',
                destinationPath: `${this.options.componentName}.module.js`,
                replace: {
                    componentPath: `${convertToKebabCase(this.options.componentName)}.component`
                }
            }, {
                templatePath: 'states.js',
                destinationPath: `${this.options.componentName}.states.js`
            }];
        }
        writing() {
            super.writing();
        }
    }

    module.exports = componentGenerator;
})();
