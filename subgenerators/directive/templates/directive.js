(function() {
    'use strict';

    angular.module('<%= moduleName %>').directive('<%= componentName %>', <%= componentName %>);

    <%= componentName %>.$inject = [];

    function <%= componentName %>() {
        return {
            scope: {},
            restrict: 'E',
            link: function($scope) {
                
            }
        };
    }
})();
