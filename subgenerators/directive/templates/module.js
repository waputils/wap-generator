(function() {
    'use strict';

    angular.module('<%= moduleName %>', []);
    require('./<%= componentName %>.directive.js');
})();