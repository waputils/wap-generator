(() => {
    'use strict';

    const frontEndModuleGenerator = require('../../baseClasses/frontEndComponents');

    class componentGenerator extends frontEndModuleGenerator {
        initialize() {
            super.initialize();

            this.templatesConfigs = [{
                templatePath: 'directive.js',
                destinationPath: `${this.options.componentName}.directive.js`,
            }, {
                templatePath: 'module.js',
                destinationPath: `${this.options.componentName}.module.js`
            }];
        }
        writing() {
            super.writing();
        }
    }

    module.exports = componentGenerator;
})();
