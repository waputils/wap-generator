(() => {
    'use strict';

    const frontEndModuleGenerator = require('../../baseClasses/frontEndComponents');

    class componentGenerator extends frontEndModuleGenerator {
        initialize() {
            super.initialize();

            this.templatesConfigs = [{
                templatePath: 'factory.js',
                destinationPath: `${this.options.componentName}.factory.js`
            }];
        }
        writing() {
            super.writing();
        }
    }

    module.exports = componentGenerator;
})();
