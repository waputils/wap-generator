(function() {
    'use strict';

    angular.module('<%= moduleName %>').factory('<%= componentName %>', <%= componentName %>);

    <%= componentName %>.$inject = [];

    function <%= componentName %>() {
        var <%= componentName %> = {

        };

        return <%= componentName %>;

        // Define factory functions below.


    }
})();