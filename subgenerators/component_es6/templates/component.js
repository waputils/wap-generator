(() => {
    'use strict';

    angular
        .module('<%= moduleName %>')
        .component('<%= componentName %>', {
            templateUrl: '<%= templateUrl %>',
            controllerAs: 'Ctrl',
            bindings: {
            },
            controller: () => {
                'ngInject';

                class <%= className %>Ctrl {
                    constructor() {
                        throw new Error('NotImplemented');
                    }
    
                    $onInit() {
                        throw new Error('NotImplemented');
                    }
                }

                return new <%= className %>Ctrl();
            }
        });
})();
