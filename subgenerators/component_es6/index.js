(() => {
    'use strict';

    const frontEndModuleGenerator = require('../../baseClasses/frontEndComponents');
    const convertToKebabCase = require('../../helpers/convertCamelToKebab');

    class componentGenerator extends frontEndModuleGenerator {
        initialize() {
            super.initialize();

            this.templatesConfigs = [{
                templatePath: 'component.js',
                destinationPath: `${this.options.componentName}.component.js`,
                replace: {
                    templateUrl: `${convertToKebabCase(this.options.componentName)}.html`
                }
            }, {
                templatePath: 'template.html',
                destinationPath: `${this.options.componentName}.html`,
                replace: {
                    componentName: convertToKebabCase(this.options.componentName)
                }
            }, {
                templatePath: '_styles.scss',
                destinationPath: `_${this.options.componentName}.scss`,
                replace: {
                    componentName: convertToKebabCase(this.options.componentName)
                }
            }, {
                templatePath: 'module.js',
                destinationPath: `${this.options.componentName}.module.js`,
                replace: {
                    componentName: convertToKebabCase(this.options.componentName)
                }
            }];
        }
        writing() {
            super.writing();
        }
    }

    module.exports = componentGenerator;
})();
