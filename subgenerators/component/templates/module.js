(function() {
    'use strict';

    angular.module('<%= moduleName %>', []);
    require('./<%= componentName %>.component.js');
})();