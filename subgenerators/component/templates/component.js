(function() {
    'use strict';

    angular
        .module('<%= moduleName %>')
        .component('<%= componentName %>', {
            templateUrl: '<%= templateUrl %>',
            controller: <%= componentName %>Controller,
            controllerAs: 'Ctrl',
            bindings: {
            }
        });

    <%= componentName %>Controller.$inject = [];

    function <%= componentName %>Controller() {
        var self = this;
        self.$onInit = onInit;
        self.$onDestroy = onDestroy;

        function onInit() {
            throw new Error('NotImplemented');
        }

        function onDestroy() {
            // eslint-disable-next-line consistent-this
            self = null;
        }
    }
})();
