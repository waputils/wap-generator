(() => {
    'use strict';

    angular
        .module('<%= moduleName %>')
        .factory('<%= componentName %>', () => {
            'ngInject';

            class <%= className %> {
                constructor() {
                    throw new Error('NotImplemented');
                }
            }

            return new <%= className %>();
        });
})();