# README #

### How to set up ###
0. Install Yeoman if not present (sudo npm install -g yo)
1. npm install
2. npm link

### How to use ###
- For front end components:  ```yo wap:fe```  optional args are ```{template type} {module name} {component name}```
- For microservices:  ```yo wap:ms```
